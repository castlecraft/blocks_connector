# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Blocks Connector",
			"color": "#B784A7",
			"icon": "fa fa-cloud",
			"type": "module",
			"label": _("Blocks Connector")
		}
	]
