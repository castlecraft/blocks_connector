import frappe
import json
import requests
import base64
from six.moves.urllib.parse import unquote, quote

def oauth_login(redirect_to):
	blocks_connector_settings = frappe.get_doc("Blocks Connector Settings")
	uid = frappe.generate_hash()
	payload = {
		'uid': uid,
	}

	if redirect_to:
		payload["redirect_to"] = redirect_to

	state = str_to_b64(json.dumps(payload))
	redirect_uri = frappe.request.host_url.strip("/")
	redirect_uri += '/api/method/blocks_connector.callback'
	redirect_uri = quote(redirect_uri, safe='')
	frappe.cache().set_value("oauth_login_state:{0}".format(state.decode("utf-8")), state, expires_in_sec=120)
	redirect = blocks_connector_settings.authorization_endpoint + '?client_id=' + blocks_connector_settings.client_id
	redirect += '&redirect_uri=' + redirect_uri
	redirect += '&scope=' + blocks_connector_settings.scope.replace(' ', '%20')
	redirect += '&response_type=code'
	redirect += '&state=' + state.decode('utf-8')
	frappe.local.response["type"] = "redirect"
	frappe.local.response["location"] = redirect


def callback(code=None, state=None):
	blocks_connector_settings = frappe.get_doc("Blocks Connector Settings")
	get_info_via_oauth(blocks_connector_settings, code, state)


def str_to_b64(string):
	"""
	Returns base64 encoded string
	"""
	return base64.b64encode(string.encode('utf-8'))


def b64_to_str(b64):
	"""
	Returns base64 decoded string
	"""
	return base64.b64decode(b64).decode('utf-8')


def get_info_via_oauth(app, code, state):
	request_state = json.loads(b64_to_str(state))
	cached_state = frappe.cache().get_value("oauth_login_state:{0}".format(state), expires=True)
	local_state = json.loads(b64_to_str(cached_state.decode("utf-8")))

	if(request_state.get("uid") != local_state.get("uid")):
		frappe.respond_as_web_page(_("Invalid Request"), _("Invalid Token"), http_status_code=417)
		return

	data = 'client_id=' + app.client_id
	data += '&redirect_uri=' + app.redirect_uri
	data += '&grant_type=authorization_code'
	data += '&code=' + code
	data += '&scope=' + app.scope.replace(' ', '%20')

	headers = {'Content-Type': 'application/x-www-form-urlencoded'}

	response = requests.post(
					app.token_endpoint,
					data=data,
					headers=headers)

	token = response.json()
	redirect_to = local_state.get('redirect_to')
	create_or_retrieve_user(token.get("access_token"), app.userinfo_endpoint, redirect_to)


def create_or_retrieve_user(token, user_endpoint, redirect_to):
	if not token:
		frappe.local.response["type"] = "redirect"
		frappe.local.response["location"] = '/login'
		return

	save = False
	headers = {"Authorization": "Bearer " + token}
	response = requests.get(
			user_endpoint,
			headers=headers)
	response = response.json()
	email = response.get("email")
	if not frappe.db.exists("User", email):
		save = True
		user = frappe.new_doc("User")
		user.update({
			"doctype":"User",
			"first_name": response.get("name"),
			"email": email,
			"enabled": 1,
			"send_welcome_email": False,
			"new_password": frappe.generate_hash(email),
			"user_type": "Website User",
		})
	else:
		user = frappe.get_doc("User", email)
		if not user.enabled:
			frappe.respond_as_web_page(_('Not Allowed'), _('User {0} is disabled').format(user.email))
			return False

	if save:
		user.flags.ignore_permissions = True
		user.flags.no_welcome_mail = True
		user.save()

	frappe.local.login_manager.user = user.email
	frappe.local.login_manager.post_login()

	# because of a GET request!
	frappe.db.commit()

	if not redirect_to:
		if user.user_type == "System User":
			redirect_to = "/desk"
		if user.user_type == "Website User":
			redirect_to = "/me"

	redirect_to = redirect_to.replace("&amp;", "&")
	frappe.local.response["type"] = "redirect"
	frappe.local.response["location"] = redirect_to

@frappe.whitelist(allow_guest=True)
def check_session():
	frappe.local.response["session"] = frappe.session.user != 'Guest'
