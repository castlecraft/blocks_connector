# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import base64
import json
import requests
from six.moves.urllib.parse import unquote
import frappe
from frappe import _
from .oauth import callback as _callback,\
					oauth_login as _oauth_login, \
					str_to_b64, b64_to_str

__version__ = '0.0.1'

@frappe.whitelist(allow_guest=True)
def oauth_login(redirect_to=None):
    _oauth_login(redirect_to)

@frappe.whitelist(allow_guest=True)
def callback(code=None, state=None):
	_callback(code, state)
